import boto3
import json
'''
Example python script to run from dev jenkins. 
It assumes the ec2 instance role and gets details about 
the ec2 instance that is running dev jenkins
'''
ec2 = boto3.client('ec2', region_name='us-east-1')
ec2_id = "i-0eb7d4ed872bde6f0"
res = ec2.describe_instances(
  InstanceIds = [ec2_id]
)
print(res)